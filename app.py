from pyramid.config import Configurator
from pyramid.view import view_config
from pyramid.request import Request
from pyramid.response import FileResponse
from wsgiref.simple_server import make_server


@view_config(route_name="index", request_method="GET")
def get_index(request: Request):
    return FileResponse("index.html", request=request)


@view_config(
    route_name="hello",
    renderer="json",
    request_method="POST",
    openapi=True,
)
def post_hello(request: Request):
    vreq = request.openapi_validated
    return dict(
        errors=vreq.errors,
        body=vreq.body,
        parameters=dict(
            query=vreq.parameters.query,
            header=dict(vreq.parameters.header.items()),
            cookie=vreq.parameters.cookie,
            path=vreq.parameters.path,
        ),
        security=vreq.security,
        server=vreq.server,
        operation=vreq.operation,
        path=vreq.path,
    )


def app():
    with Configurator() as config:
        config.add_route("index", "/")
        config.include("pyramid_openapi3")
        config.pyramid_openapi3_spec('openapi.yaml')
        config.pyramid_openapi3_add_explorer()
        config.add_route("hello", "/hello/{p_string}")
        config.scan(".")
        return config.make_wsgi_app()


if __name__ == "__main__":
    print("visit frontend at http://127.0.0.1:6543/")
    print("visit api explorer at http://127.0.0.1:6543/docs/")
    server = make_server("127.0.0.1", 6543, app())
    server.serve_forever()
